import React, { Component } from 'react';
import { checkpoints as getCheckpoints } from '../services/api';

export default class DatasetSelector extends Component {
  constructor(props) {
    super(props);
    this.state = { value: null, dataset: null, min: null, max: null, step: null };
  }

  onChange = (e) => {
    const value = parseInt(e.target.value, 10);
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    if (this.state.step !== null && this.state.dataset === this.props.dataset) {
      return <section className="control slider checkpoint-slider">
        <label htmlFor="checkpointSelector">Trainingsfase</label>
        <input type="range" min={this.state.min} step={this.state.step} max={this.state.max} value={this.state.value} onChange={this.onChange} />
      </section>
    }
    else {
      const dataset = this.props.dataset;
      getCheckpoints(dataset).then((data) => {
        const min = data[0],
              step = data[1] - data[0],
              max = data[data.length-1],
              value = max;

        this.setState({ dataset, min, max, step, value });
        this.props.onChange(value);
      });
      return <section className="control checkpoint-selector">
        <label>Opleidings niveaus worden geladen...</label>
      </section>
    }
  }
}