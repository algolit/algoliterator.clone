import React, { Component } from 'react';

export default class LengthSlider extends Component {
  constructor(props) {
    super(props);
    this.min = 10;
    this.max = 250;
    this.step = 10;
    this.state = { value: 50 };
  }

  onChange = (e) => {
    const value = parseInt(e.target.value, 10);
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    return <section className="control length-slider">
      <label htmlFor="checkpointSelector">Textlength</label>
      <input type="range" min={this.min} step={this.step} max={this.max} value={this.state.value} onChange={this.onChange} />
      <input type="number" size="4" value={this.state.value} onChange={this.onChange} />
    </section>
  }
}