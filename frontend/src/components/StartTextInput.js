import React, { Component } from 'react';

export default class StartTextInput extends Component {
  constructor(props) {
    super(props);
    // this.state = { value: (this.props.value) ? this.props.value : '' };
    this.state = { value: '' };
  }

  updateState = (e) => {
    const value = e.target.value;
    this.setState({ value });
    this.props.onChange(value)
  }
  
  render = () => {
    return <section className="control">
      <input type="text" value={this.props.value} onChange={ this.updateState} />
    </section>
  }
}