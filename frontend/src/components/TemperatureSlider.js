import React, { Component } from 'react';

export default class DatasetSelector extends Component {
  constructor(props) {
    super(props);
    this.min = 0.1;
    this.max = 2;
    this.step = .1;
    this.state = { value: 0.5 };
  }

  onChange = (e) => {
    const value = parseFloat(e.target.value);
    this.setState({ value });
    this.props.onChange(value);
  }

  render = () => {
    return <section className="control slider length-slider">
      <label htmlFor="checkpointSelector">Onvoorspelbaarheid</label>
      <input type="range" min={this.min} step={this.step} max={this.max} value={this.state.value} onChange={this.onChange} />
      {/* <input type="number" size="4" value={this.state.value} onChange={this.onChange} min={this.min} step={this.step} max={this.max} /> */}
    </section>
  }
}