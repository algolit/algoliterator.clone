import React, { Component } from 'react';

class Fragment extends Component {
  select = () => {
    this.props.onSelect(this.props.text);
  }

  focus = () => {
    this.props.onFocus(this.props.text)
  }

  blur = () => {
    this.props.onBlur()
  }

  render = () => {
    return <section className="fragment" onClick={this.select} onMouseEnter={this.focus} onMouseLeave={this.blur}>{ this.props.text }</section>
  }
}

export default class FragmentSelector extends Component {
  select = (v) => {
    this.props.onSelect(v);
  }

  fragmentFocus = (v) => {
    this.props.onFragmentFocus(v);
  }

  fragmentBlur = (v) => {
    this.props.onFragmentBlur(v);
  }

  render = () => {
    return <section className="fragmentSelector">
      {this.props.fragments.map((v, k) => 
        <Fragment key={k} text={v.text} onSelect={this.select} onFocus={this.fragmentFocus} onBlur={this.fragmentBlur} />)}
    </section>
  }
}