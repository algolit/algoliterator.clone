import React, { Component } from 'react';

export default class AboutPanel extends Component {
  close = () => { this.props.onClose() }

  render = () => {
    return <section id="about">
      <button onClick={this.close} id="closeAbout">&nbsp;X&nbsp;</button>
      <section class="grow">
        <p>
        <strong>De Algoliterator</strong> is een schrijfrobot, een neuraal netwerk getraind op het werk van Felix Timmermans.
        </p>
        <p>
        In 'trainingsfase' kan je verschillende fases in het trainingsproces kiezen. Zo zie je hoe het algoritme leerde schrijven à la Timmermans. Het leerniveau van de Algoliterator is beperkt, omdat zelfs het volledige werk van Timmermans maar een fractie is van de hoeveelheid tekst die nodig is om de Algoliterator 'mensenzinnen' te laten schrijven.
        De robot begint met een originele zin van Felix Timmermans, en schrijft drie mogelijke vervolgzinnen in zijn eigen dadaïstische Timmermansstijl.
        </p>
        <p>
        Je kan zijn voorstel ook herschrijven en de Algoliterator laten reageren. Als je de tekst goed vindt, stuur hem dan naar Zora die jouw geremixt fragment voorleest!
        </p>
      </section>
      <section>
        <p className="small">
          De Algoliterator is een project van Gijs de Heij en An Mertens voor de Publiek Domein Dag 2018.
        </p>
        <p className="small">
          Met ondersteuning van Muntpunt. Dank aan Bea Jacquemyn, Michael van Dingenen &amp; Kristof de Win.
        </p>
      </section>
    </section>
  }
}