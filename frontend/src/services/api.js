// const API_URL = "http://10.182.246.119:5556/";
// const API_URL = "http://192.168.1.55:5556/";
const API_URL = "http://127.0.0.1:5556/";

// const API_URL = "/api/";

export const api_url = (tail) => {
  return `${API_URL}${tail}`;
}

const get = (url) => {
  const promise = new Promise((resolve, reject) => {
    fetch(api_url(url), {
      method: "GET"
    }).then((response) => {
      if (response.ok) {
          response.json()
          .then(data => resolve(data))
          .catch(reject);
      }
      else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

// TODO generalize API call function
const post = (url, data) => {
  const promise = new Promise((resolve, reject) => {
    let postData = new FormData();

    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        postData.append(key, data[key]);
      }
    }

    fetch(api_url(url), {
      method: "POST",
      body: postData
    }).then((response) => {
      if (response.ok) {
        response.json()
          .then((data) => resolve(data))
          .catch(reject);
      } else {
        reject();
      }
    }).catch(reject);
  });

  return promise;
}

export const datasets = () => get('datasets')

export const startsentences = () => get('startsentences')

export const checkpoints = (dataset) => get(`checkpoints/${dataset}`)

export const generate = (data) => post('generate', data)

export const generateMultiple = (data) => {
  if (data.start_text) {
    const promise = new Promise((resolve, reject) => {
      const start_length = data.start_text.length;
      data.length = data.length + start_length;
      
      post('generateMultiple', data)
        .then((result) => {
          resolve(result.map((v) => {
            v.text = v.text.substr(start_length).replace(/\s+$/, '');
            return v;
          }));
        })
        .catch(reject);
      });

    return promise;
  } else {
    return post('generateMultiple', data);
  }
}

export const proclaim = (data) => post('proclaim', data)