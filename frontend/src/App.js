import React, { Component } from 'react';
import { generateMultiple as generateFragments, proclaim as proclaimText, startsentences as getStartsentences } from './services/api.js';
import { CheckpointSelector, TemperatureSlider, FragmentSelector, AboutPanel } from './components';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    // const dataset = 'felix-timmermans-better';
    // const dataset = 'jules-verne';
    const dataset = 'tiny-shakespeare';
    // const dataset = 'felix-timmermans-20180523';
    this.state = { dataset, checkpoint: null, length: 150, temperature: 0.5, start_text_length: 150, text: '', text_preview: '', generating: false, fragments: [], start_text: '', startsentences: [] };
    getStartsentences().then((data) => {
      this.setState({ startsentences: data})
      this.pickStartsentence()
    })
  }

  // onDataset = (dataset) => this.setState({ dataset })

  onCheckpoint = (checkpoint) => this.setState({ checkpoint })

  onLength = (length) => this.setState({ length })

  onStartText = (start_text) => this.setState({ start_text })

  proclaim = () => {
    const text = this.state.text;
    proclaimText({ text })
  }

  pickStartsentence = () => {
    if (this.state.startsentences.length) {
      var pick = Math.floor(Math.random() * this.state.startsentences.length);
      this.setState({ text: this.state.startsentences[pick] });
    }
  }

  generateFragments = () => {
    const dataset = this.state.dataset;
    const checkpoint = this.state.checkpoint;
    const length = this.state.length;
    const temperature = this.state.temperature;
    const text = this.state.text;
    const start_text = text.substr(text.length - this.state.start_text_length);
    const amount = 3;
    this.setState({ generating: true, fragments: [] })
    generateFragments({ dataset, checkpoint, length, temperature, amount, start_text })
      .then((data) => this.setState({ fragments: data, generating: false }))
      .catch(() => {
        this.setState({ generating: false })
      })
  }

  extendText = (text) => {
    this.setState({
      text: this.state.text + text,
      text_preview: '',
      fragments: [] 
    });
  }

  setTextPreview = (text) => {
    this.setState({
      text_preview: this.state.text + text
    });
  }

  clearTextPreview = (text) => {
    this.setState({
      text_preview: ''
    });
  }

  manualEdit = (e) => {
    this.setState({
      text: e.target.value
    });
  }

  updateStartTextLength = (l) => {
    this.setState({
      start_text_length: l
    });
  }

  setTemperature = (t) => {
    this.setState({
      temperature: parseFloat(t)
    });
  }

  showAbout = () => {
    this.setState({aboutVisible: true});
  }
  
  closeAbout = () => {
    this.setState({aboutVisible: false});
  }

  render() {
    let generateButton;
    if (this.state.generating) {
      generateButton = <section className="control">
        <button disabled>Bezig met genereren...</button>
      </section>;
    }
    else {
      generateButton = <section className="control">
        <button onClick={this.generateFragments}>Genereer zinnen</button>
      </section>;
    }
    const about = (this.state.aboutVisible) ? <AboutPanel onClose={this.closeAbout} /> : '';
    return (
      <div className="App">
        <nav className="controls">
          {/* <DatasetSelector onChange={this.onDataset} /> */}
          <section className="control">
            <button onClick={this.pickStartsentence}>Nieuwe startzin</button>
          </section>
          <CheckpointSelector dataset={this.state.dataset} onChange={this.onCheckpoint} />
          <TemperatureSlider onChange={this.setTemperature} />
          {/* <LengthSlider onChange={this.onLength} /> */}
          {/* <LookbackSlider onChange={this.updateStartTextLength} /> */}
          {generateButton}
          <section className="control">
            <button onClick={this.proclaim}>Stuur de tekst naar Zora</button>
          </section>
          <section className="control">
            <button onClick={this.showAbout}>?</button>
          </section>
        </nav>
        <FragmentSelector fragments={this.state.fragments} onSelect={this.extendText} onFragmentFocus={this.setTextPreview} onFragmentBlur={this.clearTextPreview} />
        <section className="generatedText">
          <textarea id="previewText" value={this.state.text_preview} />
          <textarea value={this.state.text} onChange={this.manualEdit} />
          {/* <article className="generatedText" contentEditable onBlur={ this.manualEdit }>
            {this.state.text.split('\n').map((p, k) => <p key={k}>{p}</p>)} 
          </article> */}
        </section>
        {about}
      </div>
    );
  }
}

export default App;
