gpu = -1  # -1 to use the cpu, otherwise often 0
checkpoint_dir = '/absolute/path/to/directory/containing/checkpoint/collections'
sample_bin = '/absolute/path/to/sample.lu'
default_start_text = None
proclamation_ip = '0.0.0.0'
proclamation_port = 5001
baseurl = ''