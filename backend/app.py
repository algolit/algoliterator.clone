# -*- coding: utf-8 -*-
from flask import Flask, request, send_file
from flask_socketio import SocketIO, send, emit
import glob
import os.path
import json
import re
import subprocess
import requests
import settings
import socket

app = Flask(__name__)
socketio = SocketIO(app)

checkpoint_patt = re.compile('checkpoint_(\d+)\.t7')

app.config["APPLICATION_ROOT"] = settings.baseurl


def perform_proclamation(text):
    IP = '10.182.246.115'
    PORT = 13883

    socketio.emit('proclamation', text)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
    s.connect((settings.proclamation_ip, settings.proclamation_port))
    s.send(text.replace('…', '...').encode())
    s.close()

def get_datasets():
    return [os.path.basename(p) for p in glob.glob(os.path.join(settings.checkpoint_dir, '*'))]


def get_checkpoints(dataset):
    checkpoints = []

    for p in glob.glob(os.path.join(settings.checkpoint_dir, dataset, '*.t7')):
        filename = os.path.basename(p)
        m = checkpoint_patt.search(filename)
        if m:
            checkpoints.append(int(m.group(1)))

    return checkpoints
    #return [int(re.findos.path.splitext(os.path.basename(p))[0]) for p in glob.glob(os.path.join(checkpoint_dir, dataset, '*.t7'))]


def get_checkpoint_path(dataset, checkpoint):
    return os.path.join(settings.checkpoint_dir, dataset, 'checkpoint_{}.t7'.format(checkpoint))


@app.after_request
def add_headers(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')

    return response


@app.route('/datasets', methods=["GET"])
def datasets():
    datasets = get_datasets()
    datasets.sort()
    return json.dumps(datasets)


@app.route('/checkpoints/<dataset>', methods=["GET"])
def checkpoints(dataset):
    dataset = re.sub(r'[^\w-]', '', dataset)
    checkpoints = get_checkpoints(dataset)
    checkpoints.sort()
    return json.dumps(checkpoints)


def generateFragment(dataset, checkpoint, length, temperature, start_text = None):
    checkpoint_file = get_checkpoint_path(dataset, checkpoint)

    if start_text:
        args = ['th', settings.sample_bin, '-gpu', str(settings.gpu), '-checkpoint', checkpoint_file, '-length', str(length), '-temperature', str(temperature), '-start_text', start_text.replace('\r\n', '\n').replace('\t', ' ')]
    else:
        args = ['th', settings.sample_bin, '-gpu', str(settings.gpu), '-checkpoint', checkpoint_file, '-length', str(length), '-temperature', str(temperature)]

    print(start_text, args)

    return subprocess.check_output(args, cwd=os.path.dirname(settings.sample_bin)).decode()


@app.route('/generate', methods=["POST"])
def generate():
    dataset = re.sub(r'[^\w-]', '', request.form['dataset'])
    checkpoint = int(request.form['checkpoint'])
    length = int(request.form['length'])
    temperature = float(request.form['temperature'])
    start_text = unicode(request.form['start_text']) if 'start_text' in request.form else None
    
    return json.dumps({'text': generateFragment(dataset, checkpoint, length, temperature, start_text)})

@app.route('/generateMultiple', methods=["POST"])
def generateMultiple():
    dataset = re.sub(r'[^\w-]', '', request.form['dataset'])
    checkpoint = int(request.form['checkpoint'])
    length = int(request.form['length'])
    temperature = float(request.form['temperature'])
    start_text = str(request.form['start_text']) if 'start_text' in request.form else None
    amount = min(int(request.form['amount']), 10)

    data = [{'text': generateFragment(dataset, checkpoint, length, temperature, start_text)} for i in range(amount)]

    return json.dumps(data)

@app.route('/proclaim', methods=["POST"])
def proclaim():
    text = str(request.form['text'])
    return json.dumps({'answer': perform_proclamation(text)})

@app.route('/startsentences', methods=["GET"])
def startsentences():
    return send_file('startsentences.json')

@app.route('/subtitle', methods=["GET"])
def subtitle ():
    return """<html>
        <head>
            <title>Subtitle</title>
            <style>
		@font-face {
		  font-family: 'Karla';
		  src: url('/static/media/Karla-Regular.fd3f938a.ttf') format('truetype');
		  font-weight: 300;
		}

        @font-face {
		  font-family: 'Karla';
		  src: url('/static/media/Karla-Bold.b45be274.ttf') format('truetype');
		  font-weight: 600;
		}

		body {
		  padding: 4em 3em 3em 3em;
		}

                #proclamation {
                    font-size: 3.5em;
                    font-family: 'Karla', sans-serif;
                }
            </style>
        </head>
        <body>
            <section id="proclamation">
                <section id="header">
                    <p>
                        <strong>De Algoliterator</strong> is een schrijfrobot, een neuraal netwerk getraind op het werk van Felix Timmermans.
                    </p>
                </section>
                <section id="text">
                </section>
            </section>
            <script type="text/javascript" src="/static/js/socketio.js"></script>
            <script type="text/javascript" charset="utf-8">
                var socket = io.connect(':5555');
                socket.on('proclamation', function(text) {
                    var proclamation = document.querySelector('#text');
                    while (proclamation.firstChild) {
                       proclamation.removeChild(proclamation.firstChild);
                    }
                    var paragraphs = text.split("\\n");
                    for (var i=0;i<paragraphs.length;i++) {
                        var textNode = document.createTextNode(paragraphs[i]),
                            paragraph = document.createElement('p');
                        paragraph.appendChild(textNode);
                        proclamation.appendChild(paragraph);
                    }
                });
            </script>
            <script>
                function autoscroll() {
                    if (window.scrollY < (document.body.scrollHeight - window.innerHeight)) {
                        window.scrollBy(0, 5);
                    } else {
                        window.scrollTo(0, 0);
                    }
                }

                window.setInterval(autoscroll, 25);
            </script>
        </body>
        </html>"""

if __name__ == '__main__':
    # app.run(host="0.0.0.0", port=5556, debug=True)
    socketio.run(app, port=5556, debug=True)
# torch-rnn/checkpoint-*
