#!/usr/bin/env python

import socket
import sys

IP = '0.0.0.0'
PORT = 5557

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

sock.bind((IP, PORT))

while True: 
    data, addr = sock.recvfrom(4096)
    sys.stdout.write(data)
    sys.stdout.flush()
    
