import re
import random
import json

sentences = []

with open('tiny-shakespeare.txt', 'r') as f:
  for line in f.readlines():
    m = re.search('(\w+\W+){{{}}}'.format(random.randint(7,12)), line)
    if m:
      sentences.append(m.group(0))

with open('tiny-shakespeare.startsentences.json', 'w') as w:
  w.write(json.dumps(sentences))